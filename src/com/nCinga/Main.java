package com.nCinga;

public class Main {

    public static void main(String[] args) throws InterruptedException {
	// write your code here
        Account account = new Account("Charith",50000);
        Thread threadOne = new Thread(new TransactionDeposit(account,5000));
        Thread threadTwo = new Thread(new TransactionWithdraw(account,500));
        Thread threadThree = new Thread(new TransactionWithdraw(account,3000));
        Thread threadFour = new Thread(new TransactionDeposit(account,15000));

        threadOne.start();
        threadTwo.start();
        threadThree.start();
        threadFour.start();
        threadOne.join();
        threadTwo.join();
        threadThree.join();
        threadFour.join();


        account.viewBalance();


    }
}
