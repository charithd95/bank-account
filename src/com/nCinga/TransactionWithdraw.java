package com.nCinga;

public class TransactionWithdraw implements Runnable {

    private final Account account;
    private  final int amount;

    public TransactionWithdraw(Account account, int amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void run() {
        account.withdraw(amount);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
