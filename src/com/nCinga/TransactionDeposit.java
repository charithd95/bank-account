package com.nCinga;

public class TransactionDeposit implements Runnable {
    private final Account account;
    private  final int amount;

    public TransactionDeposit(Account account, int amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void run() {
                account.deposit(amount);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    }
}
