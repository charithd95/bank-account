package com.nCinga;

public class Account {
    private String name;
    private int accountNumber=0;
    private static int autoInNumber=0;
    private int balance;

    public Account(String name, int balance) {
        this.accountNumber=autoIncrement();
        this.name = name;
        this.balance = balance;
    }
    private int autoIncrement(){ return ++autoInNumber;
    }
    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }
    public synchronized void viewBalance(){
        System.out.println("Account Balance : "+balance);
    }
    public synchronized void deposit(int amount){
        balance=balance+amount;
        System.out.println("Deposit Successful");
    }
    public synchronized void withdraw(int amount){
        if (balance>=amount){
            balance=balance-amount;
            System.out.println("Withdraw Successful");

        }
        else{
            System.out.println("Not have enough balance to withdraw that much");
        }
    }

}
